libpdf-reuse-perl (0.39-4) unstable; urgency=medium

  * Remove generated test file via debian/clean. (Closes: #1049024)
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Thu, 07 Mar 2024 18:53:14 +0100

libpdf-reuse-perl (0.39-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libpdf-reuse-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 22 Oct 2022 10:54:38 +0100

libpdf-reuse-perl (0.39-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 16 Jun 2022 23:53:24 +0100

libpdf-reuse-perl (0.39-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sat, 02 Jan 2021 18:53:49 +0100

libpdf-reuse-perl (0.39-1) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Florian Schlichting ]
  * New upstream version 0.39 (closes: #514737)
  * Add upstream metadata
  * Update copyright years
  * Add build-dependency on Test::Deep (for t/Reuse.t)
  * Bump dh compat to level 9
  * Declare compliance with Debian Policy 3.9.8

 -- Florian Schlichting <fsfs@debian.org>  Tue, 11 Oct 2016 22:00:40 +0200

libpdf-reuse-perl (0.36-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Robin Sheat ]
  * New upstream release.
  * Added debian/source/format, modified debian/rules to not invoke quilt
    manually.
  * Updated Standards-Version
  * Removed fix-pod.patch and uninitialized_warning.patch as they're upstream

  [ gregor herrmann ]
  * Bump debhelper compatibility level to 8.
  * Remove unversioned 'perl' from Depends.
  * debian/rules: don't install empty PDF::Reuse::Util manpage.

 -- Robin Sheat <robin@catalyst.net.nz>  Tue, 16 Dec 2014 17:32:39 +1300

libpdf-reuse-perl (0.35-2) unstable; urgency=low

  * debian/control:
    - switch Vcs-Browser field to ViewSVN
    - mention module name in long description
  * Fix "Uninitialized value $string in unpack" by creating
    uninitialized_warning.patch which checks for $string before using it;
    thanks to Patrick Matthäi for the bug report and the patch
    (closes: #506214).
  * Add patch fix-pod.patch, avoids an error from pod2man.
  * Add quilt framework and debian/README.source.

 -- gregor herrmann <gregoa@debian.org>  Sun, 07 Dec 2008 22:03:53 +0100

libpdf-reuse-perl (0.35-1) unstable; urgency=low

  * New upstream release.
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed: XS-
    Vcs-Svn fields.
  * debian/control: add libfont-ttf-perl, libtext-pdf-perl to
    Build-Depends-Indep and Recommends.
  * debian/control: add /me to Uploaders.
  * Use dist-based URL in debian/watch.
  * debian/control: Move libcompress-zlib-perl from Build-Depends to
    Build-Depends-Indep.
  * debian/copyright: point to specific upstream source location and update
    copyright information.
  * Remove empty debian/patches directory and quilt/patchutils dependencies.
  * Switch from cdbs to debhelper 7 (debian/{rules,control.compat}).
  * Don't install README any more.
  * Set Standards-Version to 3.8.0 (no changes).

 -- gregor herrmann <gregoa@debian.org>  Sun, 06 Jul 2008 23:03:30 +0200

libpdf-reuse-perl (0.33-1) unstable; urgency=low

  * Initial release. (Closes: #408064: ITP: libpdf-reuse-perl -- Reuse
    and mass produce PDF documents - Debian Bug report logs)

 -- Vincent Danjean <vdanjean@debian.org>  Wed, 24 Jan 2007 16:26:27 +0100
